/*
 * Copyright (c) 2017 CommonsWare, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.fdroid.apputil.common;

import android.support.annotation.NonNull;
import java.io.File;

/**
 * Generic interface for ways to download the content identified by a URL
 * to a designated file.
 */
public interface FileDownloadStrategy {
  /**
   * Callback to find out the results of the attempted download
   */
  interface Callback {
    /**
     * Called if the download succeeded. Cheese and cake for everyone!
     *
     * @param url the URL that was successfully downloaded
     */
    void onSuccess(@NonNull String url);

    /**
     * Called if the download failed, with some explanation for the failure
     *
     * @param url the URL that was not successfully downloaded
     * @param t what went wrong
     */
    void onError(@NonNull String url, @NonNull Throwable t);
  }

  /**
   * Requests to download a particular bit of content from the Web to a
   * specific file.
   *
   * @param url the URL (https preferred!) to download
   * @param dest the file where the results should go
   * @param cb a callback to find out the results
   */
  void download(@NonNull String url, @NonNull File dest, @NonNull Callback cb);
}
