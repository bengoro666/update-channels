/*
 * Copyright (c) 2017 CommonsWare, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.fdroid.apputil.common;

import android.support.annotation.NonNull;
import java.io.File;
import java.io.InputStream;

/**
 * Generic interface for ways to download the content identified by a URL,
 * supplying an InputStream for you to read the content from.
 */
public interface StreamDownloadStrategy {
  /**
   * Callback to find out the results of the attempted download
   */
  interface Callback {
    /**
     * Called if the download succeeded. Cheese and cake for everyone!
     *
     * @param url the URL that was successfully downloaded
     * @param content the content located at that URL
     */
    void onSuccess(@NonNull String url, @NonNull InputStream content) throws Exception;

    /**
     * Called if the download failed, with some explanation for the failure
     *
     * @param url the URL that was not successfully downloaded
     * @param t what went wrong
     */
    void onError(@NonNull String url, @NonNull Throwable t);
  }

  /**
   * Requests to download a particular bit of content from the Web
   *
   * @param url the URL (https preferred!) to download
   * @param cb a callback to find out the results
   */
  void download(@NonNull String url, @NonNull Callback cb);
}
