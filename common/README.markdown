AppUtils Common
=================
This library contains a series of interfaces and classes that abstract away
the details of downloading something from the Internet to an `InputStream`
or `File`. Other libraries in this project rely upon these strategy interfaces
to be able to download content.

There are two interfaces:

- `StreamDownloadStrategy`, which, given a URL, arranges to download it and provides
you with an `InputStream` from which to read the bytes. This is good for cases
where you have a consumer that can work with the `InputStream`, as this may save
you having to write the data to a file.

- `FileDownloadStrategy`, which, given a URL, arranges to download it to a
`File` that you designate.

Usage: `OkHttp3DownloadStrategy`
--------------------------
The recommended strategy class to use is `OkHttp3DownloadStrategy`. As the
name suggests, this uses OkHttp3 as its implementation. This allows you to supply
your own configured `OkHttpClient.Builder`, so you can set up caching strategies,
downloads over Tor using NetCipher, and so forth.

There are two constructors:

- A zero-argument constructor, which uses a default `OkHttpClient`

- A one-argument constructor, taking an `OkHttpClient.Builder`

`OkHttp3DownloadStrategy` supports both the `StreamDownloadStrategy` and
`FileDownloadStrategy` interfaces.

**NOTE**: To use this class, you need to add OkHttp3 version 3.6.0 or higher
to your project. This will *not* be added automatically, just by having
a transitive dependency upon the `common` module.

Usage: `HURLDownloadStrategy`
-----------------------------
`HURLDownloadStrategy` uses `HttpURLConnection` to download the content. As with
`OkHttp3DownloadStrategy`, `HURLDownloadStrategy` supports both the `StreamDownloadStrategy` and
`FileDownloadStrategy` interfaces. It does not depend upon a third-party 
library, the way that `OkHttp3DownloadStrategy` does. However, it is not
configurable. The only constructor is a zero-argument constructor.

Usage: `ManagedDownloadStrategy`
-------------------------------
`ManagedDownloadStrategy` uses `DownloadManager` for downloading the content.
This has the benefit of automatically recovering from various problems, such
as connectivity interruptions. On the other hand:

- It is more complicated to use
- It only supports `FileDownloadStrategy`
- It can only download to external storage
- It requires the `WRITE_EXTERNAL_STORAGE` permission

There are two constructors. Both require a `Context`. Only only takes the
`Context`. The other also takes a `ManagedDownloadStrategy.RequestBuilder`
instance. That interface has one method: `buildRequest()`. You are given
a `Uri` to the content and a `Uri` to where the content should be downloaded,
and your job is to create and configure a `DownloadManager.Request` object
to do the download. The one-parameter constructor uses an internal
`ManagedDownloadStrategy.RequestBuilder` that:

- Allows downloading over both mobile data and WiFi
- Allows downloading over roaming
- Allows downloading over metered connections
- Does not require the device to be charging
- Does not require the device to be idle

In your own `ManagedDownloadStrategy.RequestBuilder`, you could configure
the `DownloadManager.Request` differently, if you so choose.

Notes on Testing
----------------
To be able to run the `ManagedDownloadStrategy` tests on Android 6.0+ devices,
you will need to grant the runtime permission for storage to the test app.
