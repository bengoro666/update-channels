/*
 * Copyright (c) 2017 CommonsWare, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.fdroid.apputil.parser;

import android.annotation.TargetApi;
import android.support.annotation.NonNull;
import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.annotation.Target;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import static android.os.Build.VERSION_CODES.HONEYCOMB;

/**
 * Parser of JSON-formatted catalogs, using Android's JsonReader
 */
@TargetApi(HONEYCOMB)
public class JsonCatalogParser extends CatalogParser {
    private static final String KEY_REPO = "repo";
    private static final String KEY_APPS = "apps";
    private static final String KEY_TIMESTAMP = "timestamp";
    private static final String KEY_VERSION = "version";
    private static final String KEY_NAME = "name";
    private static final String KEY_ICON = "icon";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_MIRRORS = "mirrors";
    private static final String KEY_CATEGORIES = "categories";
    private static final String KEY_SUGGESTED_VERSION_CODE = "suggestedVersionCode";
    private static final String KEY_ISSUE_TRACKER = "issueTracker";
    private static final String KEY_LICENSE = "license";
    private static final String KEY_SOURCE_CODE = "sourceCode";
    private static final String KEY_ADDED = "added";
    private static final String KEY_PACKAGE_NAME = "packageName";
    private static final String KEY_BITCOIN = "bitcoin";
    private static final String KEY_SUMMARY = "summary";
    private static final String KEY_WEB_SITE = "webSite";
    private static final String KEY_PACKAGES = "packages";
    private static final String KEY_APK_NAME = "apkName";
    private static final String KEY_HASH = "hash";
    private static final String KEY_HASH_TYPE = "hashType";
    private static final String KEY_MIN_SDK_VERSION = "minSdkVersion";
    private static final String KEY_TARGET_SDK_VERSION = "targetSdkVersion";
    private static final String KEY_SIG = "sig";
    private static final String KEY_SIZE = "size";
    private static final String KEY_USES_PERMISSION = "uses-permission";
    private static final String KEY_VERSION_CODE = "versionCode";
    private static final String KEY_VERSION_NAME = "versionName";
    private static final String KEY_NATIVECODE = "nativecode";
    private static final String KEY_LAST_UPDATED = "lastUpdated";

    /**
     * @{inheritDoc}
     */
    @Override
    public Catalog parse(@NonNull InputStream in, ApplicationFilter filter)
            throws Exception {
        ZipInputStream zis = new ZipInputStream(in);
        Catalog catalog = null;

        for (ZipEntry entry; (entry = zis.getNextEntry()) != null; ) {
            if ("index-v1.json".equals(entry.getName())) {
                catalog = parseJson(new InputStreamReader(zis), filter);

                break;
            }
        }

        zis.close();

        return (catalog);
    }

    private Catalog parseJson(Reader in, ApplicationFilter filter)
            throws Exception {
        JsonReader json = new JsonReader(in);
        Catalog catalog = new Catalog();
        Map<String, Application> apps = new HashMap<>();

        json.beginObject();

        while (json.hasNext()) {
            String name = json.nextName();

            if (KEY_REPO.equals(name)) {
                catalog.repo(parseRepo(json));
            } else if (KEY_APPS.equals(name)) {
                json.beginArray();

                while (json.hasNext()) {
                    Application app = parseApp(json);

                    if (filter == null || filter.isAcceptable(app)) {
                        catalog.addApp(app);
                        apps.put(app.id(), app);
                    }
                }

                json.endArray();
            } else if (KEY_PACKAGES.equals(name)) {
                json.beginObject();

                while (json.hasNext()) {
                    String id = json.nextName();
                    Application app = apps.get(id);

                    if (app == null) {
                        json.skipValue();
                    } else {
                        json.beginArray();

                        while (json.hasNext()) {
                            app.addPackage(parsePackage(json));
                        }

                        json.endArray();
                    }
                }

                json.endObject();
            } else {
                json.skipValue();
            }
        }

        json.endObject();

        return (catalog);
    }

    private Repo parseRepo(JsonReader json) throws IOException {
        Repo repo = new Repo();

        json.beginObject();

        while (json.hasNext()) {
            String name = json.nextName();

            if (KEY_TIMESTAMP.equals(name)) {
                repo.timestamp(json.nextLong());
            } else if (KEY_VERSION.equals(name)) {
                repo.version(json.nextInt());
            } else if (KEY_NAME.equals(name)) {
                repo.name(json.nextString());
            } else if (KEY_ICON.equals(name)) {
                repo.icon(json.nextString());
            } else if (KEY_DESCRIPTION.equals(name)) {
                repo.description(json.nextString());
            } else if (KEY_MIRRORS.equals(name)) {
                json.beginArray();

                while (json.hasNext()) {
                    repo.addMirror(json.nextString());
                }

                json.endArray();
            } else {
                json.skipValue();
            }
        }

        json.endObject();

        return (repo);
    }

    Application parseApp(JsonReader json) throws IOException {
        Application app = new Application();

        json.beginObject();

        while (json.hasNext()) {
            String name = json.nextName();

            if (KEY_CATEGORIES.equals(name)) {
                json.beginArray();

                while (json.hasNext()) {
                    app.addCategories(json.nextString());
                }

                json.endArray();
            } else if (KEY_SUGGESTED_VERSION_CODE.equals(name)) {
                app.marketvercode(json.nextString());
            } else if (KEY_DESCRIPTION.equals(name)) {
                app.desc(json.nextString());
            } else if (KEY_ISSUE_TRACKER.equals(name)) {
                app.tracker(json.nextString());
            } else if (KEY_LICENSE.equals(name)) {
                app.license(json.nextString());
            } else if (KEY_NAME.equals(name)) {
                app.name(json.nextString());
            } else if (KEY_SOURCE_CODE.equals(name)) {
                app.source(json.nextString());
            } else if (KEY_ADDED.equals(name)) {
                app.added(json.nextLong());
            } else if (KEY_LAST_UPDATED.equals(name)) {
                app.lastUpdated(json.nextLong());
            } else if (KEY_ICON.equals(name)) {
                app.icon(json.nextString());
            } else if (KEY_PACKAGE_NAME.equals(name)) {
                app.id(json.nextString());
            } else if (KEY_BITCOIN.equals(name)) {
                app.bitcoin(json.nextString());
            } else if (KEY_SUMMARY.equals(name)) {
                app.summary(json.nextString());
            } else if (KEY_WEB_SITE.equals(name)) {
                app.web(json.nextString());
            } else {
                json.skipValue();
            }
        }

        json.endObject();

        return (app);
    }

    private PackageSpec parsePackage(JsonReader json) throws IOException {
        PackageSpec pkg = new PackageSpec();

        json.beginObject();

        while (json.hasNext()) {
            String name = json.nextName();

            if (KEY_ADDED.equals(name)) {
                pkg.added(json.nextLong());
            } else if (KEY_APK_NAME.equals(name)) {
                pkg.apkname(json.nextString());
            } else if (KEY_HASH.equals(name)) {
                pkg.hashValue(json.nextString());
            } else if (KEY_HASH_TYPE.equals(name)) {
                pkg.hashType(json.nextString());
            } else if (KEY_MIN_SDK_VERSION.equals(name)) {
                pkg.sdkver(json.nextString());
            } else if (KEY_TARGET_SDK_VERSION.equals(name)) {
                pkg.targetSdkVersion(json.nextString());
            } else if (KEY_SIG.equals(name)) {
                pkg.sig(json.nextString());
            } else if (KEY_SIZE.equals(name)) {
                pkg.size(json.nextInt());
            } else if (KEY_USES_PERMISSION.equals(name)) {
                json.beginArray();

                while (json.hasNext()) {
                    json.beginArray();

                    while (json.hasNext()) {
                        JsonToken next = json.peek();

                        if (next == JsonToken.NULL) {
                            json.skipValue();
                        } else {
                            pkg.addPermissions(json.nextString());
                        }
                    }

                    json.endArray();
                }

                json.endArray();
            } else if (KEY_VERSION_CODE.equals(name)) {
                pkg.versioncode(json.nextInt());
            } else if (KEY_VERSION_NAME.equals(name)) {
                pkg.version(json.nextString());
            } else if (KEY_NATIVECODE.equals(name)) {
                json.beginArray();

                while (json.hasNext()) {
                    pkg.addNativecode(json.nextString());
                }

                json.endArray();
            } else {
                json.skipValue();
            }
        }

        json.endObject();

        return (pkg);
    }
}
