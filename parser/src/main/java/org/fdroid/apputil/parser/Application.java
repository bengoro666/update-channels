/*
 * Copyright (c) 2017 CommonsWare, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.fdroid.apputil.parser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Details about an application from the catalog. An application represents
 * a logical work product, identified by an ID (package name). The List
 * of PackageSpec objects represents the individual versioned artifacts for
 * that application (i.e., APK files).
 */
public class Application {
    private String id;
    private long added;
    private long lastUpdated;
    private String name;
    private String summary;
    private String icon;
    private String desc;
    private String license;
    private List<String> categories = new ArrayList<>();
    private String category;
    private String web;
    private String source;
    private String tracker;
    private String bitcoin;
    private String marketversion;
    private String marketvercode;
    private List<PackageSpec> packages = new ArrayList<>();

    public String id() {
        return (id);
    }

    void id(String id) {
        this.id = id;
    }

    public long added() {
        return (added);
    }

    void added(long added) {
        this.added = added;
    }

    public long lastUpdated() {
        return (lastUpdated);
    }

    void lastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String name() {
        return (name);
    }

    void name(String name) {
        this.name = name;
    }

    public String summary() {
        return (summary);
    }

    void summary(String summary) {
        this.summary = summary;
    }

    public String icon() {
        return (icon);
    }

    void icon(String icon) {
        this.icon = icon;
    }

    public String desc() {
        return (desc);
    }

    void desc(String desc) {
        this.desc = desc;
    }

    public String license() {
        return (license);
    }

    void license(String license) {
        this.license = license;
    }

    public List<String> categories() {
        return (categories);
    }

    void addCategories(String... categories) {
        Collections.addAll(this.categories, categories);
    }

    public String category() {
        return (category);
    }

    void category(String category) {
        this.category = category;
    }

    public String web() {
        return (web);
    }

    void web(String web) {
        this.web = web;
    }

    public String source() {
        return (source);
    }

    void source(String source) {
        this.source = source;
    }

    public String tracker() {
        return (tracker);
    }

    void tracker(String tracker) {
        this.tracker = tracker;
    }

    public String bitcoin() {
        return (bitcoin);
    }

    void bitcoin(String bitcoin) {
        this.bitcoin = bitcoin;
    }

    public String marketversion() {
        return (marketversion);
    }

    void marketversion(String marketversion) {
        this.marketversion = marketversion;
    }

    public String marketvercode() {
        return (marketvercode);
    }

    void marketvercode(String marketvercode) {
        this.marketvercode = marketvercode;
    }

    public List<PackageSpec> packages() {
        return (packages);
    }

    void addPackage(PackageSpec pkg) {
        packages.add(pkg);
    }
}
