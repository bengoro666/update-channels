F-Droid App Utils
=================
This project has a set of library modules designed to help developers integrate
with F-Droid. Specifically:

- [`installer/`](installer/README.markdown) helps you determine if the user's device
has the Play Store or F-Droid installed, and if not, helps you download and
install F-Droid

- [`updater/`](updater/README.markdown) helps you determine if your app has an
update available from your F-Droid-compatible app repository

- [`parser/`](parser/README.markdown) provides a parser for XML- and JSON-formatted
F-Droid repository catalogs

- [`common/`](common/README.markdown) contains some common utility code, notable
a generic interface (and some implementations) to download content from an
HTTPS URL and provide it to you as an `InputStream` or `File`

Details for using each of these libraries can be found in each module's corresponding
`README.markdown` file.

License
-------
The code in this project is licensed under the Apache
Software License 2.0, per the terms of the included LICENSE
file.
