/*
 * Copyright (c) 2017 CommonsWare, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.fdroid.getfdroid.demo;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import org.fdroid.apputil.common.FileDownloadStrategy;
import org.fdroid.apputil.common.HURLDownloadStrategy;
import org.fdroid.apputil.common.OkHttp3DownloadStrategy;
import org.fdroid.getfdroid.FDroidInstaller;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;

public class DemoFragment extends Fragment
        implements FileDownloadStrategy.Callback {
    private MenuItem install;
    private MenuItem notify;
    private FDroidInstaller installer;
    private ProgressBar progress;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
        setHasOptionsMenu(true);
        installer = new FDroidInstaller(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return (inflater.inflate(R.layout.activity_main, container, false));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        progress = (ProgressBar) view.findViewById(R.id.progress);
        updateStatusLabels();

        View v = view.findViewById(R.id.okhttp);

        if (v != null) {
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    installer.download(new OkHttp3DownloadStrategy(buildOkBuilder()),
                            DemoFragment.this);
                }
            });
        }

        v = view.findViewById(R.id.hurl);

        if (v != null) {
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    installer.download(new HURLDownloadStrategy(), DemoFragment.this);
                }
            });
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        updateStatusLabels();
    }

    @Override
    public void onDestroy() {
        installer.cleanup();

        super.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.actions, menu);
        install = menu.findItem(R.id.install);
        install.setEnabled(installer.getFDroidStatus() ==
                FDroidInstaller.PackageStatus.NOT_INSTALLED);
        notify = menu.findItem(R.id.notify);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        progress.setProgress(0);

        switch (item.getItemId()) {
            case R.id.okhttp:
                installer.download(new OkHttp3DownloadStrategy(buildOkBuilder()), this);
                return (true);

            case R.id.hurl:
                installer.download(new HURLDownloadStrategy(), this);
                return (true);

            case R.id.notify:
                item.setChecked(!item.isChecked());
                return (true);
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateStatusLabels() {
        if (getView() != null) {
            updateStatusLabel((TextView) getView().findViewById(
                    R.id.play),
                    R.string.msg_play, installer.getPlayStoreStatus());
            updateStatusLabel((TextView) getView().findViewById(
                    R.id.fdroid),
                    R.string.msg_fdroid, installer.getFDroidStatus());
        }
    }

    private void updateStatusLabel(TextView tv, int msgId,
                                   FDroidInstaller.PackageStatus status) {
        String statusMessage = getString(R.string.msg_invalid);

        if (status == FDroidInstaller.PackageStatus.NOT_INSTALLED) {
            statusMessage = getString(R.string.msg_not_installed);
        } else if (status == FDroidInstaller.PackageStatus.VALID) {
            statusMessage = getString(R.string.msg_validated);
        }

        tv.setText(getString(msgId, statusMessage));
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onSuccess(String url) {
        progress.setProgress(progress.getMax());

        try {
            if (notify == null || !notify.isChecked()) {
                if (!installer.install()) {
                    oops();
                }
            } else {
                PendingIntent pi = installer.buildPendingIntent(1337);
                Notification notification = new Notification.Builder(getActivity())
                        .setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS)
                        .setContentTitle(getString(R.string.notify_text))
                        .setContentIntent(pi)
                        .setSmallIcon(android.R.drawable.stat_notify_sdcard)
                        .getNotification();
                NotificationManager mgr =
                        (NotificationManager) getActivity()
                                .getSystemService(Context.NOTIFICATION_SERVICE);

                mgr.notify(1337, notification);
            }
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Exception trying to install", e);
            oops();
        }
    }

    @Override
    public void onError(String url, Throwable t) {
        Log.e(getClass().getSimpleName(), "Exception downloading F-Droid", t);

        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getActivity(), "We could not download F-Droid -- sorry!", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private void oops() {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getActivity(),
                            "Downloaded but cannot install -- sorry!",
                            Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private OkHttpClient.Builder buildOkBuilder() {
        final ProgressResponseBody.Listener progressListener =
                new ProgressResponseBody.Listener() {
                    long lastUpdateTime = 0L;

                    @Override
                    public void onProgressChange(long bytesRead,
                                                 long contentLength,
                                                 boolean done) {
                        long now = SystemClock.uptimeMillis();

                        if (now - lastUpdateTime > 1000) {
                            progress.setMax((int) contentLength);
                            progress.setProgress((int) bytesRead);
                            lastUpdateTime = now;
                        }
                    }
                };

        Interceptor nightTrain = new Interceptor() {
            @Override
            public Response intercept(Chain chain)
                    throws IOException {
                Response original = chain.proceed(chain.request());
                Response.Builder b = original
                        .newBuilder()
                        .body(
                                new ProgressResponseBody(original.body(),
                                        progressListener));

                return (b.build());
            }
        };

        int cacheSize = 20 * 1024 * 1024;
        Cache cache = new Cache(new File(getActivity().getCacheDir(), ".fdroid"),
                cacheSize);

        return (new OkHttpClient.Builder()
                .cache(cache)
                .addNetworkInterceptor(nightTrain));
    }
}
